# Generative Adversarial Networks (GANs)

Generative Adversarial Networks (GANs) are a powerful class of neural networks that are used for unsupervised learning. It was developed and introduced by Ian J. Goodfellow in 2014. GANs are basically made up of a system of two competing neural network models which compete with each other and are able to analyze, capture and copy the variations within a dataset.


First `Clone` this repository in your local computer.


`Install  Python` and `Anaconda` in in your local computer.

## Installation Steps are

```python
pip3 install --upgrade pip
pip3 install torch===1.3.1 torchvision===0.4.2 -f https://download.pytorch.org/whl/torch_stable.html #PyTorch is defined as an open source machine learning library for Python. It is used for applications such as natural language processing.

```
## cd to cloned directory

```python
conda activate  gan-demo # Using this you can create your own working directory.

jupyter notebook # this command will start jupyter notebook.

```
##`Remember for this demo graphics card  should be present in your computer`
## For Better Understanding visit
[Open Documentation](https://kibbcomindia-my.sharepoint.com/:w:/g/personal/gautam_shandilya_kibbcom_com/Ed0UIUUJepRDneDtgVMo8HcBRkPvD-vjQYQpI1LnqOAeJg?e=X0J4Cp).
